import 'package:flutter/material.dart';
import 'package:task_lister/home.dart';
import 'package:task_lister/profile.dart';
import 'package:task_lister/settings.dart';
import 'package:task_lister/utils/app_color.dart';

import 'info.dart';
import 'login_page.dart';

void main() async {
  runApp(TaskLister());
}

class TaskLister extends StatelessWidget {
  const TaskLister({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Task Lister",
      theme: ThemeData(
          primarySwatch: Colors.orange,
          appBarTheme: AppBarTheme(
              backgroundColor: AppColor.primaryColor,
              foregroundColor: Colors.black)),
      home: LoginPage(),
    );
  }
}
