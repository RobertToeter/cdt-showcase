import 'package:flutter/material.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'models/user.dart';

class SettingsPage extends StatelessWidget {
  final int userId;
  const SettingsPage({super.key, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(userId: userId,),
      ),
      body: Center(
        child: Column(
          children: [
            buildHeader(),
            buildButtons(),
          ],
        ),
      ),
    );
  }

  Widget buildHeader() {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 20),
          child: Text(
            "Settings",
            style: ThemeTextStyle.headerTextStyle,
          ),
        ),
        const CenteredDivider(length: 75),
      ],
    );
  }

  Widget buildButtons() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10),
          child: BoxButton(
              onTap: changeTheme,
              width: 200,
              height: 60,
              icon: null,
              text: "Change theme"),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: BoxButton(
              onTap: changeLanguage,
              width: 200,
              height: 60,
              icon: null,
              text: "Change language"),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: BoxButton(
              onTap: changeNotifications,
              width: 200,
              height: 60,
              icon: null,
              text: "Notification settings"),
        ),
        Container(
          margin: EdgeInsets.all(10),
          child: BoxButton(
              onTap: deleteAllTasks,
              width: 200,
              height: 60,
              icon: null,
              text: "Delete all tasks"),
        ),
      ],
    );
  }

  void changeTheme() {}

  void changeLanguage() {}

  void changeNotifications() {}

  void deleteAllTasks() {}
}
