import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/database/task_list_db.dart';
import 'package:task_lister/models/task.dart';
import 'package:task_lister/models/user.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/extended_task_item.dart';
import 'package:task_lister/widgets/extended_task_list_item.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';
import 'package:task_lister/widgets/input_field.dart';

import 'models/tasklist.dart';

class SearchPage extends StatefulWidget {
  final int userId;
  const SearchPage({super.key, required this.userId});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final searchController = TextEditingController();
  final taskDB = TaskDB();
  final listDB = TaskListDB();

  Future<List<Task>>? futureTasks;
  Future<List<Tasklist>>? futureLists;
  List<Task> _tasks = [];
  List<Tasklist> _lists = [];

  @override
  void initState() {
    // _loadInitialTasks();
    // _loadInitialLists();
    fetchTasks();
    fetchLists();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(userId: widget.userId,),
      ),
      body: FutureBuilder<List<Task>>(
        future: futureTasks,
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(child: CircularProgressIndicator(),);
          } else {
            _tasks = snapshot.data!;
            return FutureBuilder(future: futureLists, builder: (context, snapshot){
              if(snapshot.connectionState == ConnectionState.waiting){
                return Center(child: CircularProgressIndicator(),);
              } else {
                _lists = snapshot.data!;
                return buildBody();
              }
            });
          }
        },
      )
    );
  }

  Widget buildBody(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.topLeft,
          child: const BackButton(),
        ),
        topBar(),
        const SizedBox(
          height: 10,
        ),
        CenteredDivider(
          length: (MediaQuery.sizeOf(context).width / 5 * 2),
          thickness: 1,
        ),
        drawItems(),
      ],
    );
  }

  Widget topBar() {
    return Row(
      // crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          //Filler space
          width: MediaQuery.sizeOf(context).width / 6,
        ),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 5 * 3,
          child: InputField(controller: searchController, hintText: "Search"),
        ),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 6,
          child: IconButton(
              onPressed: filter,
              icon: const Icon(
                Icons.filter_alt_outlined,
                size: 40,
              )),
        ),
      ],
    );
  }

  Widget drawItems() {
    return Expanded(
      child: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: combineItems(),
          );
        },
      ),
    );
  }

  List<Widget> combineItems(){
    List<Widget> result = [];
    for(Task task in _tasks){
      if(task.userId == widget.userId && !task.inList){
        result.add(ExtendedTaskItem(task: task));
      }
    }
    for(Tasklist list in _lists){
      if(list.userId == widget.userId){
        result.add(ExtendedTaskListItem(list: list));
      }
    }
    return result;
  }

  _loadInitialTasks() async {
    final response = await rootBundle.loadString('assets/mockTasks.json');
    final List<dynamic> decodedTasks = jsonDecode(response) as List;
    final List<Task> tasks = decodedTasks.map((task) {
      return Task.fromJson(task);
    }).toList();

    setState(() {
      _tasks = tasks;
    });
  }

  _loadInitialLists() async {
    final response = await rootBundle.loadString('assets/mockLists.json');
    final List<dynamic> decodedLists = jsonDecode(response) as List;
    final List<Tasklist> lists = decodedLists.map((task) {
      return Tasklist.fromJson(task);
    }).toList();

    setState(() {
      _lists = lists;
    });
  }

  void filter() {}

  void fetchTasks(){
    setState(() {
      futureTasks = taskDB.fetchAll();
    });
  }

  void fetchLists(){
    setState(() {
      futureLists = listDB.fetchAll();
    });
  }
}
