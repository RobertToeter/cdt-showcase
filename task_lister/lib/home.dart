import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task_lister/create_task.dart';
import 'package:task_lister/create_task_list.dart';
import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/database/task_list_db.dart';
import 'package:task_lister/database/user_db.dart';
import 'package:task_lister/models/task.dart';
import 'package:task_lister/models/tasklist.dart';
import 'package:task_lister/search.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/custom_icon_button.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';
import 'package:task_lister/widgets/task_item.dart';
import 'package:task_lister/widgets/task_list_item.dart';

class HomePage extends StatefulWidget {
  final int userId;

  HomePage({super.key, required this.userId});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final taskDB = TaskDB();
  final listDB = TaskListDB();
  final userDB = UserDB();

  Future<List<Task>>? futureTasks;
  Future<List<Tasklist>>? futureLists;
  List<Task> _tasks = [];
  List<Tasklist> _lists = [];

  @override
  void initState() {
    // _loadInitialTasks();
    // _loadInitialLists();
    fetchTasks();
    fetchLists();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(
          userId: widget.userId,
        ),
      ),
      body: FutureBuilder<List<Task>>(
        future: futureTasks,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else {
            _tasks = snapshot.data!;
            return FutureBuilder(
                future: futureLists,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (snapshot.data == null) {
                      return Column(
                        children: [
                          SizedBox(
                            height: MediaQuery.sizeOf(context).height / 4 * 3,
                            child: Center(
                              child: Text(
                                "Create a task to get started",
                                style: ThemeTextStyle.subHeaderTextStyle,
                              ),
                            ),
                          ),
                          buildButtons(context),
                        ],
                      );
                    }
                    _lists = snapshot.data!;
                    return buildBody();
                  }
                });
          }
        },
      ),
    );
  }

  Widget buildBody() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: SingleChildScrollView(
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Today"),
                ),
                Column(
                  children: getTasksToday(),
                ),
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Later"),
                ),
                Column(
                  children: getTasksLater(),
                ),
              ],
            ),
          )),
          buildButtons(context)
        ],
      ),
    );
  }

  Widget buildButtons(context) {
    return SizedBox(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomIconButton(
              size: 50,
              onPressed: () {
                goToSearch(context);
              },
              icon: const Icon(
                Icons.search,
                size: 30,
              )),
          CustomIconButton(
              size: 75,
              onPressed: () {
                goToCreate(context);
              },
              icon: const Icon(
                Icons.add,
                size: 50,
              )),
          CustomIconButton(
              onPressed: share,
              icon: const Icon(
                Icons.share,
                size: 30,
              ),
              size: 50),
        ],
      ),
    );
  }

  List<Widget> getTasksToday() {
    List<Widget> tasksToday = [];

    for (Task task in _tasks) {
      if (task.deadline.day == DateTime.now().day &&
          !task.completed &&
          task.userId == widget.userId &&
          !task.inList) {
        tasksToday.add(TaskItem(task: task));
      }
    }

    for (Tasklist list in _lists) {
      if (list.deadline.day == DateTime.now().day &&
          list.completion() != 100 &&
          list.userId == widget.userId) {
        tasksToday.add(TaskListItem(list: list));
      }
    }

    return tasksToday;
  }

  List<Widget> getTasksLater() {
    List<Widget> tasksLater = [];

    for (Task task in _tasks) {
      if (task.deadline.day != DateTime.now().day &&
          !task.completed &&
          task.userId == widget.userId &&
          !task.inList) {
        tasksLater.add(TaskItem(task: task));
      }
    }

    for (Tasklist list in _lists) {
      if (list.deadline.day != DateTime.now().day &&
          list.completion() != 100 &&
          list.userId == widget.userId) {
        tasksLater.add(TaskListItem(list: list));
      }
    }

    return tasksLater;
  }

  _loadInitialTasks() async {
    final response = await rootBundle.loadString('assets/mockTasks.json');
    final List<dynamic> decodedTasks = jsonDecode(response) as List;
    final List<Task> tasks = decodedTasks.map((task) {
      return Task.fromJson(task);
    }).toList();

    setState(() {
      _tasks = tasks;
    });
  }

  _loadInitialLists() async {
    final response = await rootBundle.loadString('assets/mockLists.json');
    final List<dynamic> decodedLists = jsonDecode(response) as List;
    final List<Tasklist> lists = decodedLists.map((task) {
      return Tasklist.fromJson(task);
    }).toList();

    setState(() {
      _lists = lists;
    });
  }

  void goToCreate(context) {
    //Show pop up
    showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              // title: Text("Create item"),
              children: [
                Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(10.0),
                      child: Text(
                        "Create new...",
                        style: ThemeTextStyle.subHeaderTextStyle,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    BoxButton(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CreateTaskListPage(
                                      userId: widget.userId,
                                    )));
                      },
                      width: 125,
                      height: 50,
                      text: "Task list",
                      icon: const Icon(
                        Icons.list_alt,
                        size: 35,
                      ),
                    ),
                    BoxButton(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    CreateTaskPage(userId: widget.userId)));
                      },
                      width: 125,
                      height: 50,
                      text: "Task",
                      icon: Icon(Icons.task),
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text("Cancel"))
                  ],
                ),
              ],
            ));
  }

  void share() {}

  void goToSearch(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SearchPage(
                  userId: widget.userId,
                )));
  }

  void fetchTasks() {
    setState(() {
      futureTasks = taskDB.fetchAll();
    });
  }

  void fetchLists() {
    setState(() {
      futureLists = listDB.fetchAll();
    });
  }
}
