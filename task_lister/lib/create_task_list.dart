import 'package:date_time_picker_selector/date_time_picker_selector.dart';
import 'package:flutter/material.dart';
import 'package:task_lister/database/task_list_db.dart';
import 'package:task_lister/models/tasklist.dart';
import 'package:task_lister/utils/app_color.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'models/task.dart';

class CreateTaskListPage extends StatefulWidget {
  final int userId;

  const CreateTaskListPage({super.key, required this.userId});

  @override
  State<CreateTaskListPage> createState() => _CreateTaskListPageState();
}

class _CreateTaskListPageState extends State<CreateTaskListPage> {
  final listDB = TaskListDB();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController taskNameController = TextEditingController();
  final TextEditingController taskDescriptionController =
      TextEditingController();
  DateTime deadline = DateTime.now();
  int id = 8;
  List<Task> tasks = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(
          userId: widget.userId,
        ),
      ),
      body: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            height: 40,
            child: const BackButton(),
          ),
          SizedBox(
            height: 100,
            child: Column(
              children: [
                SizedBox(
                  width: MediaQuery.sizeOf(context).width / 3 * 2,
                  child: TextField(
                    controller: nameController,
                    textAlign: TextAlign.center,
                    style: ThemeTextStyle.headerTextStyle,
                    decoration:
                        const InputDecoration(hintText: "Task list name"),
                  ),
                ),
                const CenteredDivider(length: 100),
              ],
            ),
          ),
          buildItems(context),
        ],
      ),
    );
  }

  Widget buildItems(context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            createDatePicker(context),
            const SizedBox(
              height: 15,
            ),
            buildDescription(context),
            const SizedBox(
              height: 15,
            ),
            buildTasks(context),
            const SizedBox(
              height: 10,
            ),
            BoxButton(
              onTap: () {
                saveItem(context);
              },
              width: 140,
              height: 60,
              text: "Save task",
              style: ThemeTextStyle.subHeaderTextStyle,
            )
          ],
        ),
      ),
    );
  }

  Widget createDatePicker(context) {
    return Column(
      children: [
        Text(
          "Starting date: ",
          style: ThemeTextStyle.textTextStyle,
        ),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 4 * 3,
          child: DateTimePicker(
            initialValue: DateTime.now().toLocal().toString(),
            type: DateTimePickerType.dateTimeSeparate,
            firstDate: DateTime(2000),
            lastDate: DateTime(2100),
            dateLabelText: 'Date',
            timeLabelText: 'Time',
            icon: const Icon(Icons.event),
            style: ThemeTextStyle.textTextStyle,
            onChanged: (value) {
              setState(() {
                deadline = DateTime.parse(value);
                for (Task task in tasks) {
                  task.deadline = deadline;
                }
              });
            },
          ),
        ),
      ],
    );
  }

  Widget buildDescription(context) {
    return SizedBox(
      height: 150,
      child: Column(
        children: [
          Text(
            "Description",
            style: ThemeTextStyle.subHeaderTextStyle,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.sizeOf(context).width / 5 * 4,
                height: 200,
                child: TextFormField(
                  controller: descriptionController,
                  style: ThemeTextStyle.textTextStyle,
                  autocorrect: true,
                  minLines: 2,
                  maxLines: 10,
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildTasks(context) {
    return SizedBox(
      height: 200,
      child: Column(
        children: [
          const Text(
            "Tasks",
            style: TextStyle(fontSize: 22),
          ),
          const CenteredDivider(length: 50),
          Expanded(
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.sizeOf(context).width / 2,
                child: Column(
                  children: buildTaskList(context),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> buildTaskList(context) {
    List<Widget> taskList = [];

    for (Task task in tasks) {
      taskList.add(buildTaskItem(task, context));
    }

    taskList.add(BoxButton(
      onTap: () {
        showTaskDialog(context);
      },
      width: 80,
      height: 35,
      text: "Add",
      icon: const Icon(Icons.add),
    ));
    return taskList;
  }

  Widget buildTaskItem(Task task, context) {
    return GestureDetector(
      onTap: () {
        editTaskDialog(task, context);
      },
      child: Container(
        width: 150,
        height: 50,
        margin: const EdgeInsets.all(5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(),
          color: AppColor.primaryColor,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Text(
                    maxLines: 3,
                    task.name,
                    style: ThemeTextStyle.textTextStyle,
                  ),
                ),
              ),
            ),
            IconButton(
              onPressed: () {
                deleteTask(task, context);
              },
              icon: const Icon(
                Icons.delete,
                size: 25,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void setDeadline(DateTime deadline) {
    setState(() {
      this.deadline = deadline;
    });
  }

  void editTaskDialog(Task task, context) {
    showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(
                "Edit task",
                style: ThemeTextStyle.subHeaderTextStyle,
                textAlign: TextAlign.center,
              ),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextField(
                        controller: taskNameController,
                        textAlign: TextAlign.center,
                        style: ThemeTextStyle.textTextStyle,
                        decoration: const InputDecoration(
                          hintText: "Task name",
                          // border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextFormField(
                        controller: taskDescriptionController,
                        minLines: 2,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: "Task description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    BoxButton(
                      onTap: () {
                        setState(() {
                          task.name = taskNameController.text;
                          task.description = taskDescriptionController.text;
                        });
                        Navigator.pop(context);
                      },
                      width: 100,
                      height: 60,
                      text: "Save",
                      style: ThemeTextStyle.subHeaderTextStyle,
                    )
                  ],
                ),
              ],
            ));
  }

  void showTaskDialog(context) {
    showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(
                "Add task",
                style: ThemeTextStyle.subHeaderTextStyle,
                textAlign: TextAlign.center,
              ),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextField(
                        controller: taskNameController,
                        textAlign: TextAlign.center,
                        style: ThemeTextStyle.textTextStyle,
                        decoration: const InputDecoration(
                          hintText: "Task name",
                          // border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextFormField(
                        controller: taskDescriptionController,
                        minLines: 2,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: "Task description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    BoxButton(
                      onTap: () {
                        setState(() {
                          createTask(context);
                        });
                      },
                      width: 100,
                      height: 60,
                      text: "Add",
                      style: ThemeTextStyle.subHeaderTextStyle,
                    )
                  ],
                ),
              ],
            ));
  }

  void createTask(context) {
    tasks.add(Task(
        name: taskNameController.text,
        description: taskDescriptionController.text,
        deadline: deadline,
        id: id,
        userId: widget.userId,
        inList: true));
    taskNameController.text = "";
    taskDescriptionController.text = "";
    Navigator.pop(context);
  }

  void deleteTask(Task task, context) {
    setState(() {
      tasks.remove(task);
    });
  }

  void saveItem(context) {
    Tasklist list = Tasklist(
        name: nameController.text,
        deadline: deadline,
        description: descriptionController.text,
        userId: widget.userId,
        tasks: tasks);
    listDB.create(list: list).then((value) => list.id = value);
    Navigator.pop(context);
  }
}
