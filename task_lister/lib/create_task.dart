import 'package:date_time_picker_selector/date_time_picker_selector.dart';
import 'package:flutter/material.dart';
import 'package:task_lister/models/task.dart';
import 'package:task_lister/utils/app_color.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'database/task_db.dart';
import 'models/user.dart';

class CreateTaskPage extends StatefulWidget {
  final int userId;
  const CreateTaskPage({super.key, required this.userId});

  @override
  State<StatefulWidget> createState() => CreateTaskState();
}

class CreateTaskState extends State<CreateTaskPage> {
  final taskDB = TaskDB();
  String deadlineType = "set";
  DateTime deadline = DateTime.now();
  final descriptionController = TextEditingController();
  final nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(userId: widget.userId,),
      ),
      body: Column(
        children: [
          Container(alignment: Alignment.topLeft, child: const BackButton()),
          SizedBox(
            width: MediaQuery.sizeOf(context).width / 3 * 2,
            child: TextField(
              controller: nameController,
              style: ThemeTextStyle.headerTextStyle,
              textAlign: TextAlign.center,
              decoration: const InputDecoration(
                hintText: "Task name",
              ),
            ),
          ),
          buildItems(context),
        ],
      ),
    );
  }

  Widget buildItems(context){
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            const CenteredDivider(length: 100),
            const SizedBox(height: 25,),
            createDatePicker(context),
            const SizedBox(height: 25,),
            buildDescription(context),
            BoxButton(onTap: () {
              saveItem(context);
            }, width: 140, height: 60, text: "Save task", style: ThemeTextStyle.subHeaderTextStyle,),
          ],
        ),
      ),
    );
  }

  Widget createDatePicker(context) {
    return Column(
      children: [
        Text(
          "Starting date: ",
          style: ThemeTextStyle.textTextStyle,
        ),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 4 * 3,
          child: DateTimePicker(
            initialValue: DateTime.now().toString(),
            type: DateTimePickerType.dateTimeSeparate,
            firstDate: DateTime(2000),
            lastDate: DateTime(2100),
            dateLabelText: 'Date',
            timeLabelText: 'Time',
            icon: Icon(Icons.event),
            style: ThemeTextStyle.textTextStyle,
            onChanged: (value){
              setState(() {
                deadline = DateTime.parse(value);
              });
            },
          ),
        ),
      ],
    );
  }

  Widget buildDescription(context) {
    return Column(
      children: [
        Text(
          "Description",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        Container(
          width: MediaQuery.sizeOf(context).width / 5 * 4,
          height: 200,
          child: TextFormField(
            controller: descriptionController,
            style: ThemeTextStyle.textTextStyle,
            autocorrect: true,
            minLines: 2,
            maxLines: 10,

          ),
        ),
      ],
    );
  }

  Color getColor(String value) {
    if (value == deadlineType) {
      return AppColor.primaryColor;
    }
    return AppColor.backgroundColor;
  }

  void setDeadline(DateTime deadline) {
    setState(() {
      this.deadline = deadline;
    });
  }

  void setDeadlineType(String type) {
    setState(() {
      deadlineType = type;
    });
  }

  void saveItem(context){
    Task task = new Task(name: nameController.text, description: descriptionController.text, deadline: deadline, userId: widget.userId);
    taskDB.create(task: task).then((value) => task.id = value);
    Navigator.pop(context);
  }
}
