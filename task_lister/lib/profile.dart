import 'package:flutter/material.dart';
import 'package:task_lister/database/user_db.dart';
import 'package:task_lister/login_page.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';
import 'package:task_lister/widgets/two_option_popup.dart';

import 'models/user.dart';

class ProfilePage extends StatefulWidget {
  final int userId;

  ProfilePage({super.key, required this.userId}){
  }

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final userDB = UserDB();
  Future<User>? futureUser;
  User user = User(username: "username", email: "email", password: "password");

  @override
  void initState() {
    fetchUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(
          userId: widget.userId,
        ),
      ),
      body: FutureBuilder<User>(
        future: futureUser,
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(child: CircularProgressIndicator(),);
          } else {
            if(snapshot.data == null){
              return Center(child: Text("No user data found", style: ThemeTextStyle.textTextStyle,),);
            }
            user = snapshot.data!;
            return buildBody();
          }
        },
      ),
    );
  }

  Widget buildBody(){
    return Column(
      children: [
        const SizedBox(
          height: 25,
        ),
        buildHeader(),
        buildUserInformation(context),
        const SizedBox(
          height: 20,
        ),
        buildProductivity(context),
        const SizedBox(
          height: 10,
        ),
        buildButtons(context),
      ],
    );
  }

  Widget buildHeader() {
    return Column(
      children: [
        Text(
          "Profile",
          style: ThemeTextStyle.headerTextStyle,
        ),
        Container(
          alignment: Alignment.center,
          height: 250,
          margin: EdgeInsets.all(15),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(),
              image: DecorationImage(image: AssetImage('assets/714.jpg'))),
        )
      ],
    );
  }

  Widget buildUserInformation(context) {
    return Container(
      width: MediaQuery.sizeOf(context).width / 5 * 4,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Name: ${user.username}',
            style: ThemeTextStyle.textTextStyle,
          ),
          Text(
            'Email: ${user.email}',
            style: ThemeTextStyle.textTextStyle,
          )
        ],
      ),
    );
  }

  Widget buildProductivity(context) {
    return Column(
      children: [
        Text(
          "Productivity",
          style: ThemeTextStyle.headerTextStyle,
        ),
        Container(
          height: 200,
          width: MediaQuery.sizeOf(context).width / 3 * 2,
          decoration: BoxDecoration(
              border: Border.all(),
              image: DecorationImage(
                  image: AssetImage('assets/ExampleGraph.png'),
                  fit: BoxFit.fill)),
        )
      ],
    );
  }

  Widget buildButtons(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        BoxButton(
          onTap: EditAccount,
          width: 100,
          height: 50,
          text: 'Edit',
          icon: const Icon(
            Icons.edit,
            size: 30,
          ),
        ),
        BoxButton(
          onTap: () {
            DeleteAccount(context);
          },
          width: 100,
          height: 50,
          text: 'Delete',
          icon: const Icon(
            Icons.delete,
            size: 30,
          ),
        ),
        BoxButton(
            onTap: ReportIssue,
            width: 100,
            height: 50,
            text: 'Report',
            icon: const Icon(
              Icons.report,
              size: 30,
            )),
      ],
    );
  }

  void fetchUser() {
    setState(() {
      futureUser = userDB.fetchById(widget.userId);
    });
  }

  void EditAccount() {
    //TODO: Create edit page and navigate to it from here
  }

  void DeleteAccount(context) {
    showDialog(
        context: context,
        builder: (context) => TwoOptionPopup(
              text: "Are you sure you want to delete your account?",
              destinationRoute:
                  MaterialPageRoute(builder: (context) => LoginPage()),
              onDelete: () {
                userDB.delete(widget.userId);
              },
            ));
  }

  void ReportIssue() {}
}
