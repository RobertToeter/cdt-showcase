import 'package:flutter/material.dart';
import 'package:task_lister/utils/app_color.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'models/user.dart';

class InfoPage extends StatefulWidget {
  final int userId;
  const InfoPage({super.key, required this.userId});

  @override
  State<StatefulWidget> createState() => InfoState();
}

class InfoState extends State<InfoPage> {
  List<String> values = ["tasks", "tasklists", "account", "other"];
  String value = "tasks";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(userId: widget.userId,),
      ),
      body: Column(children: [
        buildHeader(),
        buildSelectionBar(context),
        const SizedBox(
          height: 20,
        ),
        SizedBox(
            width: MediaQuery.sizeOf(context).width / 5 * 4,
            child: SizedBox(
                height: 600, child: Column(children: [buildScrollable()]))),
        const SizedBox(height: 5)
      ]),
    );
  }

  Widget buildHeader() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            "Info",
            style: ThemeTextStyle.headerTextStyle,
          ),
        ),
        const CenteredDivider(length: 75)
      ],
    );
  }

  Widget buildSelectionBar(context) {
    return Row(
      children: [
        GestureDetector(
          onTap: setTasks,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.sizeOf(context).width / 4,
            height: 50,
            decoration:
                BoxDecoration(border: Border.all(), color: getColor("tasks")),
            child: Text(
              "Tasks",
              style: ThemeTextStyle.textTextStyle,
            ),
          ),
        ),
        GestureDetector(
          onTap: setTaskLists,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.sizeOf(context).width / 4,
            height: 50,
            decoration: BoxDecoration(
                border: Border.all(), color: getColor("tasklists")),
            child: Text(
              "Task lists",
              style: ThemeTextStyle.textTextStyle,
            ),
          ),
        ),
        GestureDetector(
          onTap: setAccount,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.sizeOf(context).width / 4,
            height: 50,
            decoration:
                BoxDecoration(border: Border.all(), color: getColor("account")),
            child: Text(
              "Account",
              style: ThemeTextStyle.textTextStyle,
            ),
          ),
        ),
        GestureDetector(
          onTap: setOther,
          child: Container(
            alignment: Alignment.center,
            width: MediaQuery.sizeOf(context).width / 4,
            height: 50,
            decoration:
                BoxDecoration(border: Border.all(), color: getColor("other")),
            child: Text(
              "Other",
              style: ThemeTextStyle.textTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget buildScrollable() {
    return Expanded(
      child: SingleChildScrollView(
        physics: const PageScrollPhysics(),
        child: Column(
          children: [buildBody()],
        ),
      ),
    );
  }

  Widget buildBody() {
    if (value == values[0]) {
      // print("$value selected");
      return buildTasks();
    } else if (value == values[1]) {
      // print("$value selected");
      return buildLists();
    } else if (value == values[2]) {
      // print("$value selected");
      return buildAccount();
    } else if (value == values[3]) {
      // print("$value selected");
      return buildOther();
    } else {
      return const Center(
        child: Text("An incorrect value was somehow selected"),
      );
    }
  }

  Widget buildTasks() {
    return Column(
      children: [
        Text("Tasks", style: ThemeTextStyle.subHeaderTextStyle),
        const CenteredDivider(
          length: 100,
        ),
        Text(
          "A task is a to-do item that either needs to be completed or is already completed. A task cannot contain any subtasks. All completed tasks can be seen in the search menu when filtering on ‘completed tasks’.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to create",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A new task can be created on the homescreen by pressing the ‘+’ button on the bottom of the screen. On the pop up that then comes up, the ‘task’ button has to be pressed. A new task can then be created. After filling all the fields the ‘create’ button can be pressed to create the task",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to edit",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A task can be edited by going to the task info screen by searching for it or selecting it from the home screen. The edit button can then be pressed at the bottom of the screen. Here all elements of the items can be edited. Not filling in a field means that that element won't be changed. The changes can be saved by pressing the save button at the bottom of the screen.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to delete",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A task can be deleted by selecting the task on the home screen or by searching it and then pressing the bin icon on the task info page. After this confirm the action by pressing the ‘yes’ button on the pop up that appeared. After doing this, the task will be deleted",
          style: ThemeTextStyle.textTextStyle,
        )
      ],
    );
  }

  Widget buildLists() {
    return Column(
      children: [
        Text("Task lists", style: ThemeTextStyle.subHeaderTextStyle),
        const CenteredDivider(
          length: 75,
        ),
        Text(
          "A task list is a list comprising of multiple tasks. A task list is only completed after all subtasks have been completed.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to create",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A task list can be created by pressing the '+' button at the bottom of the home screen. The task list option then has to be selected on the pop up that appears. All the fields can then be filled in to create a task lists. New tasks can also be created here for the list. The task list will be created by pressing the 'create' button on the bottom of the screen.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to edit",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A task list can be edited by firstly selecting the list from searching for it or from the home screen. After this the edit button can be pressed to edit the list. Leaving a field empty means it won't be changed. The changes can be saved by pressing the save button at the bottom of the screen.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to delete",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "A task list can be deleted by going to the info page of the respective list by clicking on it from the home screen or searching for it. The deletion button can then be pressed at the bottom of the screen to delete the task list.",
          style: ThemeTextStyle.textTextStyle,
        )
      ],
    );
  }

  Widget buildAccount() {
    return Column(
      children: [
        Text("Account", style: ThemeTextStyle.subHeaderTextStyle),
        const CenteredDivider(
          length: 75,
        ),
        Text(
          "The user account is used to keep track of what tasks and tasks lists a user has and how much tasks the user has completed each day. For this a user has a user name to distinguish them and a password to login the user. The email is used the give the user updates on their productivity and upcoming deadlines.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "Productivity",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "The productivity of a user is defined as the amount of tasks the user has completed per day. The user productivity can be seen on the user profile in form of a line graph.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to edit",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "The user profile by going to the user profile from the fly-out menu. The edit button can then be pressed to edit the user profile. Leaving a field empty means that the field won't be changed. The changes can be saved by pressing the save button at the bottom of the screen.",
          style: ThemeTextStyle.textTextStyle,
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          "How to delete",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 150),
        Text(
          "The user profile can be deleted by pressing the delete button on the user profile page. The profile page can be reached by selecting the profile from the fly-out menu.",
          style: ThemeTextStyle.textTextStyle,
        ),
      ],
    );
  }

  Widget buildOther() {
    return Column(
      children: [
        Text("Other", style: ThemeTextStyle.subHeaderTextStyle),
        const CenteredDivider(
          length: 75,
        ),
        //TODO: Add descriptions here
      ],
    );
  }

  Color getColor(String value) {
    if (value == this.value) {
      return AppColor.primaryColor;
    } else {
      return AppColor.backgroundColor;
    }
  }

  void setTasks() {
    setState(() {
      value = values[0];
    });
  }

  void setTaskLists() {
    setState(() {
      value = values[1];
    });
  }

  void setAccount() {
    setState(() {
      value = values[2];
    });
  }

  void setOther() {
    setState(() {
      value = values[3];
    });
  }
}
