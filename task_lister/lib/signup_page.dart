import 'package:flutter/material.dart';
import 'package:task_lister/database/user_db.dart';
import 'package:task_lister/home.dart';
import 'package:task_lister/models/user.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/input_field.dart';

class SignupPage extends StatelessWidget {
  SignupPage({super.key});
  final userDB = UserDB();

  final RegExp emailExpr =
      RegExp(r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,}$');

  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final passwordRepeatController = TextEditingController();

  final _formkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              buildLogo(),
              buildForm(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLogo() {
    return Column(
      children: [
        Image.asset('assets/TaskListerLogo.png', width: 300),
        SizedBox(
          height: 24,
        )
      ],
    );
  }

  Widget buildForm(context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Form(
          key: _formkey,
          child: Column(
            children: [
              const Text(
                'Sign up',
                style: TextStyle(
                  fontSize: 34,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(
                height: 24,
              ),
              InputField(
                validator: (value) {
                  if (value != null && value.isNotEmpty && value.length < 5) {
                    return "Your username should be more than 5 characters";
                  } else if (value != null && value.isEmpty) {
                    return "Please fill in your username";
                  }
                  return null;
                },
                controller: usernameController,
                hintText: "Enter username",
              ),
              const SizedBox(
                height: 10,
              ),
              InputField(
                validator: (value) {
                  if (value != null &&
                      value.isNotEmpty &&
                      !(value.contains(emailExpr))) {
                    return "Incorrect email format";
                  } else if (value != null && value.isEmpty) {
                    return "Please fill in your email";
                  }
                  return null;
                },
                controller: emailController,
                hintText: "Enter email",
              ),
              const SizedBox(
                height: 10,
              ),
              InputField(
                validator: (value) {
                  if (value != null && value.isNotEmpty && value.length < 5) {
                    return "Your password should be more than 5 characters";
                  } else if (value != null && value.isEmpty) {
                    return "Please fill in your password";
                  }
                  return null;
                },
                controller: passwordController,
                hintText: "Enter password",
                obscure: true,
              ),
              const SizedBox(
                height: 15,
              ),
              InputField(
                validator: (value) {
                  if (value != null &&
                      value.isNotEmpty &&
                      value != passwordController.text) {
                    return "Your passwords should match";
                  } else if (value != null && value.isEmpty) {
                    return "Please fill in your password again";
                  }
                  return null;
                },
                controller: passwordRepeatController,
                hintText: "Repeat password",
                obscure: true,
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    registerUser(context);
                  },
                  child: const Text(
                    'Sign up',
                    style: TextStyle(fontSize: 24),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  void registerUser(context) {
    if (_formkey.currentState != null && _formkey.currentState!.validate()) {
      if (usernameController.text.length < 5) {
        //Username is missing or to short
        print("Username is to short!");
      }
      if (!(emailController.text.contains(emailExpr))) {
        print("Didn't pass check!");
        //Incorrect email
        print("Incorrect email format");
      }
      if (passwordController.text.length < 5 ||
          (passwordController.text.contains("[0-9]"))) {
        //Password not strong enough
        print(
            "Password should at least be 5 characters long and contain a number!");
      }
      if (passwordController.text.compareTo(passwordRepeatController.text) !=
          0) {
        //Passwords not the same
        print("Passwords do not match!");
      }

      User user = User(
          username: usernameController.text,
          email: emailController.text,
          password: passwordController.text);

      Future<int> futureId = userDB.create(user: user);
      int userId = 0;

      FutureBuilder<int>(
        future: futureId,
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(child: CircularProgressIndicator(),);
          } else {
            userId = snapshot.data!;
            return SimpleDialog(
              children: [
                Center(child: Text("Sign-up succesful!", style: ThemeTextStyle.subHeaderTextStyle,))
              ],
            );
          }
        },
      );

      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => HomePage(userId: userId)));
    }
  }
}
