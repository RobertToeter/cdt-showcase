import 'package:flutter/material.dart';
import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/database/task_list_db.dart';
import 'package:task_lister/task_list_info.dart';
import 'package:task_lister/utils/app_color.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'models/task.dart';
import 'models/tasklist.dart';

class EditTaskListPage extends StatefulWidget {
  Tasklist list;
  late TextEditingController nameController;
  late TextEditingController descriptionController;

  EditTaskListPage({super.key, required this.list}) {
    nameController = TextEditingController(text: list.name);
    descriptionController = TextEditingController(text: list.description);
  }

  @override
  State<EditTaskListPage> createState() => _EditTaskListPageState();
}

class _EditTaskListPageState extends State<EditTaskListPage> {
  final listDB = TaskListDB();
  final taskDB = TaskDB();
  final TextEditingController taskNameController = TextEditingController();
  final TextEditingController taskDescriptionController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        drawer: Drawer(
          child: FlyoutMenu(
            userId: widget.list.userId,
          ),
        ),
        body: Column(
          children: [
            Expanded(child: SingleChildScrollView(child: buildBody(context))),
          ],
        ));
  }

  Widget buildBody(context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          child: const BackButton(),
        ),
        buildTitle(),
        buildCompletion(),
        const SizedBox(
          height: 40,
        ),
        buildDeadline(context),
        const SizedBox(
          height: 40,
        ),
        buildTasks(context),
        buildDescription(context),
        BoxButton(
          onTap: () {
            saveList(context);
          },
          width: 150,
          height: 75,
          text: "Save task list",
          style: ThemeTextStyle.subHeaderTextStyle,
        )
      ],
    );
  }

  Widget buildTitle() {
    return Column(
      children: [
        SizedBox(
          width: 250,
          child: TextField(
            controller: widget.nameController,
            textAlign: TextAlign.center,
            style: ThemeTextStyle.subHeaderTextStyle,
            decoration: InputDecoration(hintText: "Task name"),
          ),
        ),
        const CenteredDivider(length: 100),
      ],
    );
  }

  Widget buildCompletion() {
    return Column(
      children: [
        Container(
          height: 150,
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(50),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(width: 2, color: getColor())),
          child: Text(
            "${widget.list.completion()}%",
            style: ThemeTextStyle.headerTextStyle,
          ),
        ),
        Text(
          "Completion",
          style: ThemeTextStyle.subHeaderTextStyle,
        )
      ],
    );
  }

  Widget buildDeadline(context) {
    return Column(
      children: [
        Text(
          "Deadline",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${widget.list.deadline}",
              style: ThemeTextStyle.deadlineTextStyle,
            ),
            IconButton(
                onPressed: () {
                  editDeadline(context);
                },
                icon: Icon(Icons.edit))
          ],
        )
      ],
    );
  }

  Widget buildTasks(context) {
    return Column(
      children: [
        Text(
          "Tasks",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 100),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 3 * 2,
          height: 200,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: buildSubTasks(),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget buildDescription(context) {
    return Column(
      children: [
        Text(
          "Description",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 100),
        SizedBox(
          height: 200,
          width: MediaQuery.sizeOf(context).width / 3 * 2,
          child: TextField(
            controller: widget.descriptionController,
            style: ThemeTextStyle.textTextStyle,
            minLines: 2,
            maxLines: 10,
            decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Task list description"),
          ),
        )
      ],
    );
  }

  List<Widget> buildSubTasks() {
    List<Widget> result = [];

    if (widget.list.tasks == []) return result;

    for (Task task in widget.list.tasks!) {
      result.add(
        buildTaskItem(task, context),
      );
    }

    result.add(BoxButton(
      onTap: () {
        showTaskDialog(context);
      },
      width: 80,
      height: 35,
      text: "Add",
      icon: const Icon(Icons.add),
    ));

    return result;
  }

  Widget buildTaskItem(Task task, context) {
    return GestureDetector(
      onTap: () {
        editTaskDialog(task, context);
      },
      child: Container(
        width: 160,
        height: 65,
        margin: const EdgeInsets.all(5),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(),
          color: AppColor.primaryColor,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Text(
                    maxLines: 3,
                    task.name,
                    style: ThemeTextStyle.textTextStyle,
                  ),
                ),
              ),
            ),
            IconButton(
              onPressed: () {
                deleteTask(task, context);
              },
              icon: const Icon(
                Icons.delete,
                size: 25,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void editTaskDialog(Task task, context) {
    showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(
                "Edit task",
                style: ThemeTextStyle.subHeaderTextStyle,
                textAlign: TextAlign.center,
              ),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextField(
                        controller: taskNameController,
                        textAlign: TextAlign.center,
                        style: ThemeTextStyle.textTextStyle,
                        decoration: const InputDecoration(
                          hintText: "Task name",
                          // border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextFormField(
                        controller: taskDescriptionController,
                        minLines: 2,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: "Task description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    BoxButton(
                      onTap: () {
                        setState(() {
                          task.name = taskNameController.text;
                          task.description = taskDescriptionController.text;
                        });
                        Navigator.pop(context);
                      },
                      width: 100,
                      height: 60,
                      text: "Save",
                      style: ThemeTextStyle.subHeaderTextStyle,
                    )
                  ],
                ),
              ],
            ));
  }

  void showTaskDialog(context) {
    showDialog(
        context: context,
        builder: (context) => SimpleDialog(
              title: Text(
                "Add task",
                style: ThemeTextStyle.subHeaderTextStyle,
                textAlign: TextAlign.center,
              ),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextField(
                        controller: taskNameController,
                        textAlign: TextAlign.center,
                        style: ThemeTextStyle.textTextStyle,
                        decoration: const InputDecoration(
                          hintText: "Task name",
                          // border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: MediaQuery.sizeOf(context).width / 2,
                      child: TextFormField(
                        controller: taskDescriptionController,
                        minLines: 2,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: "Task description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    BoxButton(
                      onTap: () {
                        setState(() {
                          createTask(context);
                        });
                      },
                      width: 100,
                      height: 60,
                      text: "Add",
                      style: ThemeTextStyle.subHeaderTextStyle,
                    )
                  ],
                ),
              ],
            ));
  }

  void editDeadline(context) {
    showDialog(
        context: context,
        builder: (context) => DatePickerDialog(
              firstDate: DateTime(2000),
              lastDate: DateTime(2100),
              initialDate: DateTime.now(),
            )).then((selectedDate) {
      if (selectedDate != null) {
        setState(() {
          widget.list.deadline = selectedDate;
        });
      }
    });
  }

  void deleteTaskFromList({required Task task}) {
    widget.list.tasks!.remove(task);
  }

  void saveList(context) {
    listDB.update(id: widget.list.id!, list: widget.list);
    Navigator.pop(context);
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => TaskListInfoPage(list: widget.list)));
  }

  void createTask(context) async {
    Task task = Task(
        name: taskNameController.text,
        description: taskDescriptionController.text,
        deadline: widget.list.deadline,
        userId: widget.list.userId,
        inList: true);
    await taskDB.create(task: task).then((value) => task.id = value);
    widget.list.tasks!.add(task);
    await listDB.update(id: widget.list.id!, list: widget.list);
    Navigator.pop(context);
  }

  void deleteTask(Task task, context) {
    setState(() {
      widget.list.tasks!.remove(task);
    });
  }

  Color getColor() {
    if (widget.list.completion() > 66) {
      return Colors.green;
    } else if (widget.list.completion() > 33) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }
}
