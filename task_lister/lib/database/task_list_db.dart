import 'package:sqflite/sqflite.dart';
import 'package:task_lister/database/database_service.dart';
import 'package:task_lister/database/task_db.dart';

import '../models/task.dart';
import '../models/tasklist.dart';


class TaskListDB {
  final tableName = 'tasklists';
  final tasksTable = 'tasks';
  final TaskDB taskDatabase = TaskDB();

  Future<void> createTable(Database database) async {
    await database.execute("""CREATE TABLE IF NOT EXISTS $tableName (
    "id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "deadline" TEXT NOT NULL, 
    "tasks" TEXT,
    "userId" INTEGER NOT NULL,
    PRIMARY KEY ("id" AUTOINCREMENT)
    );""");
  }

  Future<int> create({required Tasklist list}) async {
    final database = await DatabaseService().database;
    String taskIds = "";
    for(Task task in list.tasks!){
      await taskDatabase.create(task: task).then((value) => taskIds += "$value|");
    }
    taskIds = taskIds.substring(0, taskIds.length - 1);
    return await database.rawInsert(
      '''INSERT INTO $tableName (name, description, deadline, tasks, userId) VALUES (?,?,?,?,?)''',
      [list.name, list.description, list.deadline.toString(), taskIds, list.userId],
    );
  }

  Future<List<Tasklist>> fetchAll() async {
    final database = await DatabaseService().database;
    final lists = await database.rawQuery('''SELECT * FROM $tableName''');
    return lists.map((list) => Tasklist.fromSqfliteDatabase(list)).toList();
  }

  Future<Tasklist> fetchById(int id) async {
    final database = await DatabaseService().database;
    final list = await database.rawQuery(
      '''SELECT * FROM $tableName WHERE id = ?''', [id]);
    return Tasklist.fromSqfliteDatabase(list.first);
  }

  Future<int> update({required int id, required Tasklist list}) async {
    final database = await DatabaseService().database;
    String taskIds = "";
    for(Task task in list.tasks!){
      await taskDatabase.create(task: task).then((value) => taskIds += "$value|");
    }
    taskIds = taskIds.substring(0, taskIds.length - 1);
    return await database.update(
      tableName,
      {
        'name': list.name,
        'description': list.description,
        'deadline': list.description,
        'tasks': taskIds,
        'userId': list.userId,
      },
      where: 'id = ?',
      conflictAlgorithm: ConflictAlgorithm.rollback,
      whereArgs: [id],
    );
  }

  Future<void> delete(int id) async {
    final database = await DatabaseService().database;
    await database.rawDelete('''DELETE FROM $tableName WHERE id = ?''', [id]);
  }
}