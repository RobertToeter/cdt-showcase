import 'package:sqflite/sqflite.dart';
import 'package:task_lister/database/database_service.dart';

import '../models/user.dart';

class UserDB {
  final tableName = 'users';

  Future<void> createTable(Database database) async {
    await database.execute("""CREATE TABLE IF NOT EXISTS $tableName (
    "id" INTEGER NOT NULL,
    "username" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "picture" TEXT,
    PRIMARY KEY ("id" AUTOINCREMENT)
    );""");
  }

  Future<int> create({required User user}) async {
    final database = await DatabaseService().database;
    return await database.rawInsert(
      '''INSERT INTO $tableName (username, email, password, picture) VALUES (?,?,?,?)''',
      [user.username, user.email, user.password, user.picture],
    );
  }

  Future<List<User>> fetchAll() async {
    final database = await DatabaseService().database;
    final users = await database.rawQuery('''SELECT * FROM $tableName''');
    return users.map((user) => User.fromSqfliteDatabase(user)).toList();
  }

  Future<User> fetchById(int id) async {
    final database = await DatabaseService().database;
    final user = await database.rawQuery('''SELECT * FROM $tableName WHERE id = ?''', [id]);
    return User.fromSqfliteDatabase(user.first);
  }

  Future<int> update({required int id, required User user}) async {
    final database = await DatabaseService().database;
    return await database.update(
        tableName,
      {
        'username': user.username,
        'email': user.email,
        'password': user.password,
        'picture': user.picture
      },
      where: 'id = ?',
      conflictAlgorithm: ConflictAlgorithm.rollback,
      whereArgs: [id],
    );
  }

  Future<void> delete(int id) async {
    final database = await DatabaseService().database;
    await database.rawDelete('''DELETE FROM $tableName WHERE id = ?''', [id]);
  }
}