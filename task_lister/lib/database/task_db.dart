import 'package:sqflite/sqflite.dart';
import 'package:task_lister/database/database_service.dart';

import '../models/task.dart';

class TaskDB {
  final tableName = 'tasks';

  Future<void> createTable(Database database) async {
    await database.execute("""CREATE TABLE IF NOT EXISTS $tableName (
    "id" INTEGER NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,
    "deadline" TEXT NOT NULL,
    "userId" INTEGER NOT NULL,
    "completed" TEXT NOT NULL,
    "inList" TEXT NOT NULL,
    PRIMARY KEY ("id" AUTOINCREMENT)
    );""");
  }

  Future<int> create({required Task task}) async {
    final database = await DatabaseService().database;
    return await database.rawInsert(
      '''INSERT INTO $tableName (name, description, deadline, completed, userId, inList) VALUES (?,?,?,?,?,?)''',
      [task.name, task.description, task.deadline.toString(), task.completed, task.userId, task.inList],
    );
  }

  Future<List<Task>> fetchAll() async {
    final database = await DatabaseService().database;
    final tasks = await database
        .rawQuery('''SELECT * FROM $tableName ORDER BY deadline DESC''');
    return tasks.map((task) => Task.fromSqfliteDatabase(task)).toList();
  }

  Future<Task> fetchById(int id) async {
    final database = await DatabaseService().database;
    final task = await database
        .rawQuery('''SELECT * FROM $tableName WHERE id = ?''', [id]);
    return Task.fromSqfliteDatabase(task.first);
  }

  Future<List<Task>> fetchByName(String name) async {
    final database = await DatabaseService().database;
    final tasks = await database
        .rawQuery('''SELECT * FROM $tableName WHERE name = ?''', [name]);
    return tasks.map((task) => Task.fromSqfliteDatabase(task)).toList();
  }

  Future<int> update({required int id, required Task task}) async {
    final database = await DatabaseService().database;
    print("Updated task $id: ${task.completed}");
    return await database.update(
      tableName,
      {
        'name': task.name,
        'description': task.description,
        'deadline': task.deadline.toString(),
        'userId': task.userId,
        'completed': task.completed,
        'inList': task.inList,
      },
      where: 'id = ?',
      conflictAlgorithm: ConflictAlgorithm.rollback,
      whereArgs: [id],
    );
  }

  Future<void> delete(int id) async {
    final database = await DatabaseService().database;
    await database.rawDelete('''DELETE FROM $tableName WHERE id = ?''', [id]);
  }
}
