class User {
  String username;
  String email;
  String password;
  String? picture;
  int? id;

  User(
      {required this.username,
      required this.email,
      required this.password,
      this.id,
      this.picture});

  factory User.fromJson(Map<String, dynamic> json) {
    if (json['picture'] == null) {
      return User(
          username: json['username'],
          email: json['email'],
          password: json['password'],
          id: json['id']);
    }

    return User(
        username: json['username'],
        email: json['email'],
        password: json['password'],
        id: json['id'],
        picture: json['picture']);
  }

  factory User.fromSqfliteDatabase(Map<String, dynamic> map) => User(
      username: map['username'],
      email: map['email'],
      password: map['password'],
      id: map['id']?.toInt() ?? 0);
}
