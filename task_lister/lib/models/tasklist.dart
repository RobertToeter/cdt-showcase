import 'dart:ffi';

import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/models/task.dart';

class Tasklist {
  String name;
  DateTime deadline;
  String description;
  List<Task>? tasks;
  int userId;
  int? id;

  Tasklist(
      {required this.name,
      required this.deadline,
      required this.description,
      this.tasks,
      required this.userId,
      this.id});

  factory Tasklist.fromJson(Map<String, dynamic> json) {
    if (json['tasks'] == null || json['tasks'] == []) {
      return Tasklist(
          name: json['taskName'],
          deadline: DateTime.parse(json['deadline']),
          description: json['description'],
          userId: json['user'],
          id: json['id']);
    }
    List<Task> tasks = [];
    List<dynamic> decodedTasks = json['tasks'].toString().split(',');
    for (dynamic task in decodedTasks) {
      tasks.add(Task.fromJson(task));
    }
    return Tasklist(
        name: json['taskName'],
        deadline: DateTime.parse(json['deadline']),
        description: json['description'],
        userId: json['user'].toInt() ?? 0,
        id: json['id'].toInt() ?? 0,
        tasks: tasks);
  }

  factory Tasklist.fromSqfliteDatabase(Map<String, dynamic> map) {
    List<Task> tasks = [];

    List<String> taskIds = map['tasks'].toString().split('|');

    for(String taskId in taskIds){
      TaskDB().fetchById(int.parse(taskId)).then((value) => tasks.add(value));
    }
    // print("Deadline: ${map['deadline']}");
    print("${map['inList']}");
    return Tasklist(
      name: map['name'],
      deadline: DateTime.parse(map['deadline']),
      description: map['description'],
      userId: map['userId'].toInt() ?? 0,
      id: map['id'].toInt() ?? 0,
      tasks: tasks,
    );
  }

  List<bool> getCompleted() {
    List<bool> result = [];
    if (tasks == []) {
      return result;
    }
    for (Task task in tasks!) {
      result.add(task.completed);
    }
    return result;
  }

  int completion() {
    int total = 0;
    if (tasks == []) {
      return 0;
    }
    for (Task task in tasks!) {
      total += task.completion();
    }
    if (total == 0) return 0;
    return (total / tasks!.length).round();
  }
}
