class Task {
  String name;
  String description;
  DateTime deadline;
  bool completed;
  bool inList;
  int? id;
  int userId;

  Task(
      {required this.name,
      required this.description,
      required this.deadline,
      this.id,
      required this.userId,
      this.completed = false,
      this.inList = false});

  factory Task.fromJson(Map<String, dynamic> json) {
    return Task(
      name: json['name'],
      description: json['description'],
      deadline: DateTime.parse(json['deadline']),
      id: json['id'].toInt() ?? 0,
      completed: json['completed'] == "true",
      userId: json['userId'].toInt() ?? 0,
      inList: json['inList'] == "true",
    );
  }

  factory Task.fromSqfliteDatabase(Map<String, dynamic> map) => Task(
        name: map['name'],
        description: map['description'],
        deadline: DateTime.parse(map['deadline']),
        id: map['id']?.toInt() ?? 0,
        userId: map['userId'].toInt() ?? 0,
        completed: map['completed'] == "true",
        inList: map['inList'] == "true",
      );

  int completion() {
    if (completed) {
      return 100;
    }
    return 0;
  }
}
