import 'package:flutter/material.dart';
import 'package:task_lister/utils/app_color.dart';

class ThemeTextStyle {
  static TextStyle loginTextFieldStyle = const TextStyle(color: Colors.blueGrey);
  static TextStyle deadlineTextStyle = const TextStyle(fontSize: 20, color: Colors.red);
  static TextStyle confirmTextStyle = const TextStyle(fontSize: 20, color: Colors.green);
  static TextStyle headerTextStyle = const TextStyle(fontSize: 30, color: Colors.black);
  static TextStyle subHeaderTextStyle = const TextStyle(fontSize: 26, color: Colors.black87);
  static TextStyle textTextStyle = const TextStyle(fontSize: 18, color: Colors.black87);
}