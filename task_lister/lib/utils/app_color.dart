import 'package:flutter/material.dart';

class AppColor {
  static Color backgroundColor = const Color(0xffF2C0A2);
  static Color primaryColor = const Color(0xffF2AB27);
  static Color accentColor = const Color(0xff593122);
}