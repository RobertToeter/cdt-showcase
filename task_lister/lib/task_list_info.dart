import 'package:flutter/material.dart';
import 'package:task_lister/database/task_list_db.dart';
import 'package:task_lister/edit_task_list.dart';
import 'package:task_lister/home.dart';
import 'package:task_lister/models/tasklist.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/checked_task.dart';
import 'package:task_lister/widgets/custom_icon_button.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';
import 'package:task_lister/widgets/two_option_popup.dart';

import 'models/task.dart';

class TaskListInfoPage extends StatefulWidget {
  Tasklist list;

  TaskListInfoPage({super.key, required this.list});

  @override
  State<StatefulWidget> createState() => TaskListInfoState();
}

class TaskListInfoState extends State<TaskListInfoPage> {
  final listDB = TaskListDB();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(
          userId: widget.list.userId,
        ),
      ),
      body: Column(
        children: [
          Expanded(child: SingleChildScrollView(child: buildBody(context))),
          buildButtons(context),
        ],
      ),
    );
  }

  Widget buildBody(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          alignment: Alignment.topLeft,
          child: const BackButton(),
        ),
        Text(
          widget.list.name,
          style: ThemeTextStyle.headerTextStyle,
        ),
        buildCompleted(),
        SizedBox(
            height: 75,
            child: Text(
              "${widget.list.deadline}",
              style: ThemeTextStyle.deadlineTextStyle,
            )),
        buildTasks(),
        const SizedBox(
          height: 20,
        ),
        buildDescription(context),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget buildCompleted() {
    return Column(
      children: [
        Container(
          height: 150,
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(50),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: getColor(), width: 2),
          ),
          child: Text(
            '${widget.list.completion()}%',
            style: ThemeTextStyle.headerTextStyle,
          ),
        ),
        Text(
          'Completion',
          style: ThemeTextStyle.subHeaderTextStyle,
        )
      ],
    );
  }

  Widget buildTasks() {
    return SizedBox(
      height: 200,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text(
                    "Tasks",
                    style: ThemeTextStyle.subHeaderTextStyle,
                  ),
                  const CenteredDivider(length: 60),
                  Container(
                    alignment: Alignment.center,
                    width: MediaQuery.sizeOf(context).width / 3 * 2,
                    child: Column(
                      children: buildSubTasks(),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDescription(context) {
    return Column(
      children: [
        Text(
          "Description",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 100),
        SizedBox(
            width: MediaQuery.sizeOf(context).width / 3 * 2,
            child: Text(
              widget.list.description,
              style: ThemeTextStyle.textTextStyle,
            )),
      ],
    );
  }

  Widget buildButtons(context) {
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomIconButton(
              onPressed: () {
                deleteList(context);
              },
              icon: const Icon(
                Icons.delete,
                size: 30,
              ),
              size: 50),
          CustomIconButton(
              onPressed: editList,
              icon: const Icon(
                Icons.edit,
                size: 30,
              ),
              size: 50),
          CustomIconButton(
              onPressed: shareList,
              icon: const Icon(
                Icons.share,
                size: 30,
              ),
              size: 50),
        ],
      ),
    );
  }

  List<Widget> buildSubTasks() {
    List<Widget> result = [];

    if (widget.list.tasks == []) return result;

    for (Task task in widget.list.tasks!) {
      result.add(CheckedTask(
        task: task,
      ));
    }
    return result;
  }

  Color getColor() {
    if (widget.list.completion() > 66) {
      return Colors.green;
    } else if (widget.list.completion() > 33) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }

  void deleteList(context) {
    showDialog(
        context: context,
        builder: (context) => TwoOptionPopup(
              text: "Are you sure you want to delete ${widget.list.name}?",
              destinationRoute: MaterialPageRoute(
                  builder: (context) => HomePage(
                        userId: widget.list.userId,
                      )),
              onDelete: () {
                listDB.delete(widget.list.id!);
              },
            ));
  }

  void editList() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditTaskListPage(list: widget.list)));
  }

  void shareList() {}
}
