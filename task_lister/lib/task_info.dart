import 'package:flutter/material.dart';
import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/edit_task.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/custom_icon_button.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';
import 'package:task_lister/widgets/two_option_popup.dart';

import 'home.dart';
import 'models/task.dart';

class TaskInfoPage extends StatefulWidget {
  Task task;

  TaskInfoPage({super.key, required this.task});

  @override
  State<StatefulWidget> createState() => TaskInfoState();
}

class TaskInfoState extends State<TaskInfoPage> {
  final taskDB = TaskDB();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(
          userId: widget.task.userId,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.topLeft,
            child: BackButton(),
          ),
          buildHeader(),
          const SizedBox(
            height: 40,
          ),
          buildDescription(context),
          buildCompleted(),
          buildButtons(context),
        ],
      ),
    );
  }

  Widget buildHeader() {
    return Column(
      children: [
        Text(
          widget.task.name,
          style: ThemeTextStyle.headerTextStyle,
        ),
        Text(
          "${widget.task.deadline}",
          style: ThemeTextStyle.deadlineTextStyle,
        )
      ],
    );
  }

  Widget buildDescription(context) {
    return Flexible(
      child: Column(
        children: [
          Text(
            "Description",
            style: ThemeTextStyle.subHeaderTextStyle,
          ),
          const CenteredDivider(length: 100),
          Container(
            alignment: Alignment.center,
            width: (MediaQuery.sizeOf(context).width / 3) * 2,
            child: Text(
              widget.task.description,
              style: ThemeTextStyle.textTextStyle,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildCompleted() {
    return Container(
      height: 100,
      child: Column(
        children: [
          Text(
            "Completed",
            style: ThemeTextStyle.subHeaderTextStyle,
          ),
          Transform.scale(
            scale: 2,
            child: Checkbox(
              value: widget.task.completed,
              onChanged: (bool? value) {
                setState(() {
                  widget.task.completed = value!;
                  print("${widget.task.id}: ${widget.task.completed}");
                  taskDB.update(id: widget.task.id!, task: widget.task);
                });
              },
            ),
          )
        ],
      ),
    );
  }

  Widget buildButtons(context) {
    return Container(
      height: 100,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomIconButton(
              onPressed: () {
                deleteTask(context);
              },
              icon: const Icon(
                Icons.delete,
                size: 30,
              ),
              size: 50),
          CustomIconButton(
              onPressed: () {
                editTask(context);
              },
              icon: const Icon(
                Icons.edit,
                size: 30,
              ),
              size: 50),
          CustomIconButton(
              onPressed: shareTask,
              icon: const Icon(
                Icons.share,
                size: 30,
              ),
              size: 50),
        ],
      ),
    );
  }

  void deleteTask(context) {
    showDialog(
        context: context,
        builder: (context) => TwoOptionPopup(
              text: "Are you sure you want to delete ${widget.task.name}?",
              destinationRoute: MaterialPageRoute(
                  builder: (context) => HomePage(
                        userId: widget.task.userId,
                      )),
              onDelete: () {
                taskDB.delete(widget.task.id!);
              },
            ));
  }

  void editTask(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => EditTaskPage(
                  task: widget.task,
                )));
  }

  void shareTask() {}
}
