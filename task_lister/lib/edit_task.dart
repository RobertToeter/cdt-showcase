import 'package:flutter/material.dart';
import 'package:task_lister/database/task_db.dart';
import 'package:task_lister/task_info.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/box_button.dart';
import 'package:task_lister/widgets/centered_divider.dart';
import 'package:task_lister/widgets/fly-out_menu.dart';

import 'models/task.dart';

class EditTaskPage extends StatefulWidget {
  Task task;
  late TextEditingController nameController;
  late TextEditingController descriptionController;

  EditTaskPage({super.key, required this.task}) {
    nameController = TextEditingController(text: task.name);
    descriptionController = TextEditingController(text: task.description);
  }

  @override
  State<EditTaskPage> createState() => _EditTaskPageState();
}

class _EditTaskPageState extends State<EditTaskPage> {
  final taskDB = TaskDB();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: FlyoutMenu(userId: widget.task.userId,),
      ),
      body: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            child: const BackButton(),
          ),
          buildTitle(),
          buildDeadline(context),
          const SizedBox(
            height: 20,
          ),
          buildDescription(context),
          const SizedBox(
            height: 40,
          ),
          buildCompleted(),
          const SizedBox(
            height: 40,
          ),
          BoxButton(
            onTap: () {
              saveTask(context);
            },
            width: 150,
            height: 60,
            text: "Save task",
            style: ThemeTextStyle.subHeaderTextStyle,
          )
        ],
      ),
    );
  }

  Widget buildTitle() {
    return Column(
      children: [
        SizedBox(
          width: 250,
          child: TextField(
            controller: widget.nameController,
            textAlign: TextAlign.center,
            decoration: const InputDecoration(
              hintText: "Task name",
            ),
            style: ThemeTextStyle.headerTextStyle,
          ),
        ),
        const CenteredDivider(length: 100)
      ],
    );
  }

  Widget buildDeadline(context) {
    return Column(
      children: [
        Text(
          "Deadline",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${widget.task.deadline}",
              style: ThemeTextStyle.deadlineTextStyle,
            ),
            IconButton(
                onPressed: () {
                  editDeadline(context);
                },
                icon: Icon(Icons.edit))
          ],
        )
      ],
    );
  }

  Widget buildDescription(context) {
    return Column(
      children: [
        Text(
          "Description",
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 100),
        SizedBox(
          width: MediaQuery.sizeOf(context).width / 4 * 3,
          child: TextFormField(
            minLines: 2,
            maxLines: 10,
            controller: widget.descriptionController,
            style: ThemeTextStyle.textTextStyle,
            decoration: const InputDecoration(
                hintText: "Task description", border: OutlineInputBorder()),
          ),
        ),
      ],
    );
  }

  Widget buildCompleted() {
    return Column(
      children: [
        Text(
          'Completed',
          style: ThemeTextStyle.subHeaderTextStyle,
        ),
        const CenteredDivider(length: 100),
        Transform.scale(
          scale: 2,
          child: Checkbox(
            value: widget.task.completed,
            onChanged: (bool? value) {
              setState(() {
                widget.task.completed = value!;
                taskDB.update(id: widget.task.id!, task: widget.task);
              });
            },
          ),
        ),
      ],
    );
  }

  void editDeadline(context) {
    showDialog(
        context: context,
        builder: (context) => DatePickerDialog(
              firstDate: DateTime(2000),
              lastDate: DateTime(2100),
              initialDate: DateTime.now(),
            )).then((selectedDate) {
      if (selectedDate != null) {
        setState(() {
          widget.task.deadline = selectedDate;
        });
      }
    });
  }

  void saveTask(context) {
    taskDB.update(id: widget.task.id!, task: widget.task);
    Navigator.pop(context);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TaskInfoPage(task: widget.task)));
  }
}
