import 'package:flutter/material.dart';

class CenteredDivider extends StatelessWidget {
  final double length;
  final double thickness;
  const CenteredDivider({super.key, required this.length, this.thickness = 2});

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 25,
      color: Colors.black,
      thickness: thickness,
      indent: MediaQuery.sizeOf(context).width/2 - length,
      endIndent: MediaQuery.sizeOf(context).width/2 - length,
    );
  }
}
