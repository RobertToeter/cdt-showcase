import 'package:flutter/material.dart';

class BackButton extends StatelessWidget {
  const BackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Text(
            'back',
            style: TextStyle(fontSize: 14),
          ),
        ),
      ],
    );
  }
}
