import 'package:flutter/material.dart';

import '../utils/textfield_styles.dart';

class InputField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final FormFieldValidator<String>? validator;
  final bool obscure;

  const InputField(
      {super.key,
      required this.controller,
      required this.hintText,
      this.validator, this.obscure = false});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: validator,
      obscureText: obscure,
      onChanged: (value) {
        print('Value: $value');
      },
      decoration: InputDecoration(
          hintText: hintText,
          hintStyle: ThemeTextStyle.loginTextFieldStyle,
          border: OutlineInputBorder()),
    );
  }
}
