import 'package:flutter/material.dart';

import '../models/tasklist.dart';
import '../task_list_info.dart';
import '../utils/app_color.dart';
import '../utils/textfield_styles.dart';

class TaskListItem extends StatelessWidget {
  Tasklist list;

  TaskListItem({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        navigateToInfo(context);
      },
      child: Container(
          width: 350,
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColor.backgroundColor,
            border: Border.all(color: AppColor.accentColor),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 250,
                child: Column(
                  children: [
                    Text(
                      list.name,
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                      "${list.deadline}",
                      style: ThemeTextStyle.deadlineTextStyle,
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                      width: 75,
                      height: 75,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: getColor(), width: 2)
                      ),
                      child: Text(
                        '${list.completion()}%',
                        style: ThemeTextStyle.subHeaderTextStyle,
                      ))
                ],
              )
            ],
          )),
    );
  }

  Color getColor() {
    if (list.completion() > 66) {
      return Colors.green;
    } else if (list.completion() > 33) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }

  void navigateToInfo(context) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => TaskListInfoPage(list: list,)));
  }
}