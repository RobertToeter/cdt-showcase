import 'package:flutter/material.dart';
import 'package:task_lister/utils/app_color.dart';

class CustomIconButton extends StatelessWidget {
  final double size;
  final Function onPressed;
  final Icon icon;

  const CustomIconButton(
      {super.key,
      required this.onPressed,
      required this.icon,
      required this.size});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        onPressed();
      },
      child: Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          border: Border.all(),
          shape: BoxShape.circle,
          color: AppColor.primaryColor
        ),
        child: IconButton(
          icon: icon,
          onPressed: () {
            onPressed();
          },
        ),
      ),
    );
  }
}
