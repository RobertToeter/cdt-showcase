import 'package:flutter/material.dart';

import '../utils/textfield_styles.dart';

class TwoOptionPopup extends StatelessWidget {
  final String text;
  final MaterialPageRoute destinationRoute;
  final Function onDelete;

  const TwoOptionPopup(
      {super.key, required this.text, required this.destinationRoute, required this.onDelete});

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: [
        Container(
            margin: EdgeInsets.all(8.0),
            child: Text(
              text,
              style: ThemeTextStyle.textTextStyle,
              textAlign: TextAlign.center,
            )),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            TextButton(
                onPressed: () {
                  onDelete();
                  Navigator.pushReplacement(context, destinationRoute);
                },
                child: Text(
                  "Yes",
                  style: ThemeTextStyle.confirmTextStyle,
                )),
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text(
                  "No",
                  style: ThemeTextStyle.deadlineTextStyle,
                )),
          ],
        ),
      ],
    );
  }
}
