import 'package:flutter/material.dart';

class FlyoutItem extends StatelessWidget {
  final Icon icon;
  final String name;
  final MaterialPageRoute route;

  const FlyoutItem(
      {super.key, required this.icon, required this.name, required this.route});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacement(context, route);
            },
            icon: icon,
            iconSize: 50,
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushReplacement(context, route);
            },
              child: Text(
            "$name",
            style: TextStyle(fontSize: 28),
          ))
        ],
      ),
    );
  }
}
