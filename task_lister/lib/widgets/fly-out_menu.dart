import 'package:flutter/material.dart';
import 'package:task_lister/home.dart';
import 'package:task_lister/info.dart';
import 'package:task_lister/login_page.dart';
import 'package:task_lister/profile.dart';
import 'package:task_lister/settings.dart';
import 'package:task_lister/widgets/flyout_item.dart';

import '../models/user.dart';

class FlyoutMenu extends StatelessWidget {
  final int userId;
  FlyoutMenu({super.key, required this.userId});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          buildHeader(),
          buildItems(),
          buildFooter()
        ],
      ),
    );
  }

  Widget buildHeader() {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Container(
          margin: EdgeInsets.only(top: 100, bottom: 25),
          child: Image.asset(
            'assets/TaskListerLogo.png',
            scale: 2.5,
          ),
        ),
        const SizedBox(
          height: 50,
        )
      ],
    );
  }

  Widget buildItems() {
    return Column(
      children: [
        Container(
          decoration:
              BoxDecoration(border: Border.symmetric(horizontal: BorderSide())),
          child: FlyoutItem(
            icon: Icon(Icons.home),
            name: "Home",
            route: MaterialPageRoute(builder: (context) => HomePage(userId: userId,)),
          ),
        ),
        Container(
          child: FlyoutItem(
              icon: Icon(Icons.account_circle_rounded),
              name: "Profile",
              route: MaterialPageRoute(builder: (context) => ProfilePage(userId: userId,))),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.symmetric(
            horizontal: BorderSide(),
          )),
          child: FlyoutItem(
              icon: Icon(Icons.settings),
              name: "Settings",
              route: MaterialPageRoute(builder: (context) => SettingsPage(userId: userId,))),
        ),
        Container(
          child: FlyoutItem(
              icon: Icon(Icons.info), name: "Info", route: MaterialPageRoute(builder: (context) => InfoPage(userId: userId,))),
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.symmetric(
            horizontal: BorderSide(),
          )),
          child: FlyoutItem(
              icon: Icon(Icons.logout), name: "Log out", route: MaterialPageRoute(builder: (context) => LoginPage())),
        )
      ],
    );
  }

  Widget buildFooter(){
    return Container(
      height: 200,
      alignment: Alignment.bottomCenter,
      child: Text(
        "Version 1.0",
        style: TextStyle(fontSize: 14),
      ),
    );
  }
}
