import 'package:flutter/material.dart';

import '../utils/app_color.dart';
import '../utils/textfield_styles.dart';

class BoxButton extends StatelessWidget {
  final Function onTap;
  final double width;
  final double height;
  final Icon? icon;
  final String text;
  TextStyle? style = ThemeTextStyle.textTextStyle;

  BoxButton(
      {super.key,
      required this.onTap,
      required this.width,
      required this.height,
      this.icon,
      required this.text,
      this.style}) {
  }

  @override
  Widget build(BuildContext context) {
    if (icon == null) {
      return GestureDetector(
        onTap: () {
          onTap();
        },
        child: Container(
          width: width,
          height: height,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              border: Border.all(),
              borderRadius: BorderRadius.circular(10),
              color: AppColor.primaryColor),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                  alignment: Alignment.center,
                  width: width - 15,
                  child: Text(
                    text,
                    style: style,
                    textAlign: TextAlign.center,
                  )),
            ],
          ),
        ),
      );
    }
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        width: width,
        height: height,
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border.all(),
            borderRadius: BorderRadius.circular(10),
            color: AppColor.primaryColor),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "$text",
              style: ThemeTextStyle.textTextStyle,
            ),
            icon!
          ],
        ),
      ),
    );
  }
}
