import 'package:flutter/material.dart';
import 'package:task_lister/database/task_db.dart';

import '../models/task.dart';
import '../utils/textfield_styles.dart';

class CheckedTask extends StatefulWidget {
  Task task;

  CheckedTask({super.key, required this.task});

  @override
  State<CheckedTask> createState() =>
      _CheckedTaskState();
}

class _CheckedTaskState extends State<CheckedTask> {
  final taskDB = TaskDB();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Transform.scale(
          scale: 1.3,
          child: Checkbox(
              value: widget.task.completed,
              onChanged: (bool? value) {
                setState(() {
                  widget.task.completed = value!;
                  taskDB.update(id: widget.task.id!, task: widget.task);
                });
              }),
        ),
        Flexible(
          child: Text(
            widget.task.name,
            softWrap: true,
            style: ThemeTextStyle.textTextStyle,
          
          ),
        ),
      ],
    );
  }
}
