import 'package:flutter/material.dart';
import 'package:task_lister/utils/app_color.dart';
import 'package:task_lister/utils/textfield_styles.dart';

import '../models/task.dart';
import '../task_info.dart';
import '../task_list_info.dart';

class TaskItem extends StatelessWidget {
  Task task;
  TaskItem({super.key, required this.task});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        navigateToInfo(context);
      },
      child: Container(
          width: 350,
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColor.backgroundColor,
            border: Border.all(color: AppColor.accentColor),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 250,
                child: Column(
                  children: [
                    Text(
                      task.name,
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                      "${task.deadline}",
                      style: ThemeTextStyle.deadlineTextStyle,
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                      width: 75,
                      height: 75,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: getColor(), width: 2)
                      ),
                      child: Text(
                        '${task.completion()}%',
                        style: ThemeTextStyle.subHeaderTextStyle,
                        textAlign: TextAlign.center,
                      ))
                ],
              )
            ],
          )),
    );
  }

  Color getColor() {
    if (task.completed){
      return Colors.green;
    }
    return Colors.red;
  }

  navigateToInfo(context){
    Navigator.push(context, MaterialPageRoute(builder: (context) => TaskInfoPage(task: task)));
  }
}
