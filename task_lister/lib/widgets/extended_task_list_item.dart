import 'package:flutter/material.dart';
import 'package:task_lister/models/tasklist.dart';

import '../task_list_info.dart';
import '../utils/app_color.dart';
import '../utils/textfield_styles.dart';

class ExtendedTaskListItem extends StatelessWidget {
  Tasklist list;
  ExtendedTaskListItem({super.key, required this.list});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        navigateToInfo(context);
      },
      child: Container(
          width: 350,
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColor.backgroundColor,
            border: Border.all(color: AppColor.accentColor),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 240,
                child: Column(
                  children: [
                    Text(
                      list.name,
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                      list.description,
                      style: ThemeTextStyle.textTextStyle,
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  SizedBox(
                      width: 80,
                      child: Text(
                        "${list.deadline}",
                        style: ThemeTextStyle.deadlineTextStyle,
                      ))
                ],
              )
            ],
          )),
    );
  }

  navigateToInfo(context){
    Navigator.push(context, MaterialPageRoute(builder: (context) => TaskListInfoPage(list: list)));
  }
}
