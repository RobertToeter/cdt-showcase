import 'package:flutter/material.dart';

import '../models/task.dart';
import '../task_info.dart';
import '../task_list_info.dart';
import '../utils/app_color.dart';
import '../utils/textfield_styles.dart';

class ExtendedTaskItem extends StatelessWidget {
  Task task;
  ExtendedTaskItem({super.key, required this.task});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        navigateToInfo(context);
      },
      child: Container(
          width: 350,
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: AppColor.backgroundColor,
            border: Border.all(color: AppColor.accentColor),
          ),
          child: Row(
            children: [
              SizedBox(
                width: 240,
                child: Column(
                  children: [
                    Text(
                      task.name,
                      style: TextStyle(fontSize: 24),
                    ),
                    Text(
                      task.description,
                      style: ThemeTextStyle.textTextStyle,
                    )
                  ],
                ),
              ),
              Column(
                children: [
                  SizedBox(
                      width: 80,
                      child: Text(
                        "${task.deadline}",
                        style: ThemeTextStyle.deadlineTextStyle,
                      ))
                ],
              )
            ],
          )),
    );
  }

  navigateToInfo(context){
    Navigator.push(context, MaterialPageRoute(builder: (context) => TaskInfoPage(task: task)));
  }
}
