import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task_lister/database/user_db.dart';
import 'package:task_lister/home.dart';
import 'package:task_lister/signup_page.dart';
import 'package:task_lister/utils/textfield_styles.dart';
import 'package:task_lister/widgets/input_field.dart';

import 'models/user.dart';

class LoginPage extends StatefulWidget {
  LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final userDB = UserDB();
  Future<List<User>>? futureUsers;
  List<User> _users = [];
  final _formkey = GlobalKey<FormState>();

  @override
  void initState() {
    // _loadInitialUsers();
    fetchUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<List<User>>(
        future: futureUsers,
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return const Center(child: CircularProgressIndicator(),);
          } else {
            if(snapshot.data != null){
              _users = snapshot.data!;
            }
            return Center(
              child: Padding(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildLogo(),
                    const SizedBox(
                      height: 24,
                    ),
                    buildBody(context),
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget buildBody(context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          children: [
            buildForm(),
            const SizedBox(
              height: 15,
            ),
            buildButtons(context)
          ],
        ),
      ),
    );
  }

  Widget buildLogo() {
    return Column(
      children: [
        SizedBox(
          height: 150,
        ),
        Image.asset(
          'assets/TaskListerLogo.png',
          width: 300,
        )
      ],
    );
  }

  Widget buildForm() {
    return Form(
      key: _formkey,
      child: Column(
        children: [
          const Text(
            'Login',
            style: TextStyle(
              fontSize: 34,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          InputField(
            validator: (value) {
              if (value != null && value.isNotEmpty && value.length < 5) {
                return "Your username should be more than 5 characters";
              } else if (value != null && value.isEmpty) {
                return "Please fill in your username";
              }
              return null;
            },
            hintText: "Enter username",
            controller: usernameController,
          ),
          const SizedBox(
            height: 10,
          ),
          InputField(
            validator: (value) {
              if (value != null && value.isEmpty) {
                return "Please fill in your password";
              }
              return null;
            },
            hintText: "Enter password",
            obscure: true,
            controller: passwordController,
          )
        ],
      ),
    );
  }

  Widget buildButtons(context) {
    return Column(
      children: [
        ElevatedButton(
            onPressed: () {
              loginUser(context);
            },
            child: const Text(
              'Login',
              style: TextStyle(fontSize: 24, color: Colors.black87),
            )),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SignupPage()));
            },
            child: const Text(
              'Sign up',
              style: TextStyle(fontSize: 24, color: Colors.black87),
            )),
        const SizedBox(
          height: 50,
        )
      ],
    );
  }

  _loadInitialUsers() async {
    final response = await rootBundle.loadString('assets/mockUsers.json');
    final List<dynamic> decodedUsers = jsonDecode(response) as List;
    final List<User> users = decodedUsers.map((user) {
      return User.fromJson(user);
    }).toList();

    setState(() {
      _users = users;
    });
  }

  void loginUser(context) {
    if (_formkey.currentState != null && _formkey.currentState!.validate()) {
      if (usernameController.text.isEmpty) {
        print("Please fill in username");
      }

      if (passwordController.text.isEmpty) {
        print("Please fill in password");
      }

      User? user;
      for (User u in _users) {
        if (u.username == usernameController.text &&
            u.password == passwordController.text) {
          user = u;
        }
      }

      if (user == null) {
        showDialog(
            context: context,
            builder: (context) => SimpleDialog(
                  children: [
                    Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(8.0),
                        child: Text(
                          "User not found!",
                          style: ThemeTextStyle.textTextStyle,
                        )),
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "OK",
                          style: ThemeTextStyle.textTextStyle,
                        )),
                  ],
                ));
      } else {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => HomePage(userId: user!.id!)));
      }
    }
  }

  void fetchUsers(){
    setState(() {
      futureUsers = userDB.fetchAll();
    });
  }
}
